package edu.washington.jamys2.awty;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


public class Alarm extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String message = intent.getStringExtra("message");
        String phone = intent.getStringExtra("phone");
        Toast toast = Toast.makeText(context, phone + ": " + message, Toast.LENGTH_SHORT);
        toast.show();
    }
}
