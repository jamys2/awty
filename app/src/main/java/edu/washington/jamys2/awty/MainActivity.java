package edu.washington.jamys2.awty;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private PendingIntent pendingIntent;
    private Button start;
    private boolean check;
    private String message;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = (Button)findViewById(R.id.start);
        check = false;
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click();
            }
        });
    }

    public void click() {
        if (check) {
            start.setText("Start");
            check = false;
            stopSpam();
        } else {
            EditText phoneMessage = (EditText)findViewById(R.id.message);
            EditText phoneNumber = (EditText)findViewById(R.id.phone);
            EditText phoneInterval = (EditText)findViewById(R.id.interval);
            message = phoneMessage.getText().toString();
            phone = phoneNumber.getText().toString();
            int interval = Integer.parseInt(phoneInterval.getText().toString());
            if (!message.equals("") && !phone.equals("") && interval > 0) {
                Intent send = new Intent(MainActivity.this, Alarm.class);
                send.putExtra("message", message);
                send.putExtra("phone", phone);
                pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, send, PendingIntent.FLAG_UPDATE_CURRENT);
                start.setText("Stop");
                check = true;
                beginSpam(interval);
            }
        }
    }

    private void beginSpam(int interval) {
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int time = 1000 * 60 * interval;
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), time, pendingIntent);
        Toast toast = Toast.makeText(this, phone + ": " + message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void stopSpam() {
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pendingIntent);
        Toast toast = Toast.makeText(this, "Spam Canceled", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
